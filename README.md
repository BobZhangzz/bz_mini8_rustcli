# bz_mini8_rustcli
In this project, we are going to create a command line tool in rust.

## Project setup
1. Create a new rust project using `cargo new bz_mini8_rustcli --bin`

2. Change directory to the project folder `cd bz_mini8_rustcli`

3. Open the project in your favorite code editor.

4. Open the `src/main.rs` file and add the following code:




5. Create a new file `src/lib.rs` and add the following code:

6. Run the tests using `cargo test`
```bash
cargo test
```

7. Run the project using `cargo run`
```bash
cargo run -- upper hello
```


## screenshot of the results
- successful test 
![Alt Text](../imgs/test.png)

- successful run
![Alt Text](../imgs/run.png)




