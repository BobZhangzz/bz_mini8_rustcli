use std::env;
use std::error::Error;
use std::fs;
use bz_mini8_rustcli::process_text;

fn main() -> Result<(), Box<dyn Error>> {
    let args: Vec<String> = env::args().collect();

    if args.len() < 3 {
        eprintln!("Usage: {} <input> <input>", args[0]);
        eprintln!("input: File path or string");
        eprintln!("mode: upper or lower");
        std::process::exit(1);
    }

    let input = &args[2];
    let mode = &args[1];

    let contents = match input.ends_with(".txt") {
    true => fs::read_to_string(&input)?,
    false => input.to_string(),
    };

    let processed_contents = process_text(&contents, mode)?;

    println!("{}", processed_contents);

    Ok(())
}

